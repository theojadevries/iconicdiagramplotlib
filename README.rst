======================
 IconicDiagramPlotLib
======================

**IconicDiagramPlotLib** is a toolkit for making beautiful iconic diagram models
that represent mechanical (physical) systems. It is an add-on to the wonderful
matplotlib_ library.

Example:

.. image:: ./example.png

Getting it up and running
=========================

Prerequisites
-------------

You need an installation of `matplotlib`_, which in turn requires `python`_,
`numpy`_ and `scipy`_.

Installing
----------

We have no real installation process (yet); just download the files and put
them in the same directory as where you want your own diagrams.

Running
-------

Copy the example, edit, and run with python. You might find it handy to use
`Spyder`_ for this.

Post-processing
---------------

If you save your diagram as an svg file, your models can be edited with e.g. `inkscape`_.

Contributing
============

Submit wishes, comments, patches, etc. via bitbucket:
https://bitbucket.org/theojadevries/iconicdiagramplotlib.

.. _`matplotlib`: http://matplotlib.org/
.. _`python`: http://www.python.org/
.. _`numpy`: http://www.numpy.org/
.. _`scipy`: http://www.scipy.org/
.. _`Spyder`: https://code.google.com/p/spyderlib/
.. _`inkscape`: http://inkscape.org/


