#!/usr/bin/env python2
"""
    An example how to make use of IconicDiagramPlotLib
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   Copyright 2013-2013 Theo J.A. de Vries

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

from matplotlib import use
#uncomment next line for saving to pdf
#use('pdf')
#use('svg')
#use('agg')

from matplotlib import rcParams
rcParams['text.usetex']=False

import matplotlib.pyplot as plt
import iconicdiagramplotlib as ic

fig = ic.iconic_diagram_figure()
ax = ic.get_ic_axes(fig)


ic.fixed_world(ax, [2, 13])
ic.source_of_torque(ax, [8, 13])
ic.mass(ax, [17, 13])
ic.connector(ax,[[17,13],[23,13]])

ic.lever_less(ax,[23,13], label='$i_4$', placement='bottom')
ic.connector(ax,[[23,9],[35,9]])
ic.spring(ax, [29, 9])
ic.lever_more(ax,[35,9], label='$i_2$')

ic.connector(ax,[[35,13],[47,13]])
ic.mass(ax, [41, 13])


startpoint = [19,27]
midpoint1 = [27,27]
midpoint2 = [27,29]
midpoint3 = [41,29]
midpoint4 = [41,27]
endpoint = [47,27]
route = [startpoint, midpoint1, midpoint2, midpoint3, midpoint4, endpoint]
ic.connector(ax,route)
ic.fixed_world(ax, startpoint)
ic.source_of_displacement(ax, [23, 27], label='$x_f$')
ic.spring(ax, [32.5, 29])
ic.connector(ax,[[27,27], [27,25], [41,25], [41,27]])
ic.damper(ax, [32.5, 25], r'$d$', 'bottom')
ic.mass(ax, [41, 27])

ic.motion_summer(ax,[49,20],14)

ic.connector(ax,[[51,27],[69,27]])
ic.spring(ax, [57, 27])
ic.mass(ax, [66, 27], r'$m_1$')
ic.displacement(ax, [66, 27], r'$x$')
ic.source_of_force(ax, [75, 27])
ic.fixed_world_r(ax, [81, 27])

plt.show()
#plt.savefig('example')
