#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
    IconicDiagramPlotLib - Make beautiful iconic diagrams
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   Copyright 2013-2013 Theo J.A. de Vries

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import matplotlib.pyplot as plt
import matplotlib.path as mpath
import matplotlib.patches as mpatches
import matplotlib.lines as mlines

import numpy as np


def get_ic_axes(fig):
    ax = fig.add_subplot(111,aspect='equal',frameon=False)

    return ax
    
def add_label(pos, label, valign, halign='center', font_size=20, font_family='serif'):
    if label !='':
        plt.text(pos[0], pos[1],label, horizontalalignment=halign, verticalalignment=valign,
        fontsize=font_size, family=font_family)
    return


def mass(ax, pos, label='', placement='top'):
    """mass icon

    Draw a mass icon with the center at position

    Parameters
    ----------
    ax : mathplotlib Axes instance
        the iconic diagram
    pos : [float,float]

    Returns
    -------
    ax : mathplotlib Axes instance
        the iconic diagram
    
    Notes
    -----
    None.

    Examples
    --------
    to be done
    """
    p1 = mpatches.Rectangle((pos[0]-3,pos[1]-3), 6, 6, color='#e6e6e6', ec='k', lw=2.5, alpha=1., zorder=2) #0.4
    ax.add_patch(p1)
    if label !='':
        if placement == 'center':
            x,y = pos[0], pos[1]
            label_align = 'center'
        elif placement == 'top':
            x,y = pos[0], pos[1]+4.5
            label_align = 'bottom'
        elif placement == 'bottom':
            x,y = pos[0], pos[1]-4
            label_align = 'top'
        else:
            return ax
        add_label([x, y], label, label_align)
    return ax


def spring(ax, pos, label='', placement='top'):
    p2 = mpatches.Rectangle((pos[0]-3,pos[1]-1.5), 6, 3, color='white', ec='none', alpha=1., zorder=2) #0.4
    ax.add_patch(p2)
    x,y = np.array([[
        pos[0]-3, 
        pos[0]-2.5, 
        pos[0]-1.5, 
        pos[0]-0.5, 
        pos[0]+0.5, 
        pos[0]+1.5, 
        pos[0]+2.5, 
        pos[0]+3
        ], [
        pos[1], 
        pos[1]-1.5, 
        pos[1]+1.5, 
        pos[1]-1.5, 
        pos[1]+1.5, 
        pos[1]-1.5, 
        pos[1]+1.5, 
        pos[1], 
        ]])
    line = mlines.Line2D(x, y, lw=2.5, color='k', solid_capstyle='round', zorder=2)
    ax.add_line(line)
    if label !='':
        if placement == 'center':
            x,y = pos[0], pos[1]
            label_align = 'center'
        elif placement == 'top':
            x,y = pos[0], pos[1]+2.5
            label_align = 'bottom'
        elif placement == 'bottom':
            x,y = pos[0], pos[1]-2
            label_align = 'top'
        else:
            return ax
        add_label([x, y], label, label_align)
    return ax


def damper(ax, pos, label='', placement='top'):
    p2 = mpatches.Rectangle((pos[0]-0.75,pos[1]-1.5), 1.25, 3, color='white', ec='none', alpha=1., zorder=2) #0.4
    ax.add_patch(p2)
    x,y = np.array([[
        pos[0]+0.5, 
        pos[0]-0.75, 
        pos[0]-0.75, 
        pos[0]+0.5, 
        ], [
        pos[1]+1.5, 
        pos[1]+1.5, 
        pos[1]-1.5, 
        pos[1]-1.5, 
        ]])
    line = mlines.Line2D(x, y, lw=2.5, color='k', solid_capstyle='round', zorder=2)
    ax.add_line(line)
    x,y = np.array([[
        pos[0], 
        pos[0], 
        ], [
        pos[1]+0.75, 
        pos[1]-0.75, 
        ]])
    line = mlines.Line2D(x, y, lw=2.5, color='k', solid_capstyle='round', zorder=2)
    ax.add_line(line)
    x,y = np.array([[
        pos[0], 
        pos[0]+0.5, 
        ], [
        pos[1], 
        pos[1],
        ]])
    line = mlines.Line2D(x, y, lw=1.5, color='k', solid_capstyle='round', zorder=2)
    ax.add_line(line)
    if label !='':
        if placement == 'center':
            x,y = pos[0], pos[1]
            label_align = 'center'
        elif placement == 'top':
            x,y = pos[0], pos[1]+2.5
            label_align = 'bottom'
        elif placement == 'bottom':
            x,y = pos[0], pos[1]-2
            label_align = 'top'
        else:
            return ax
        add_label([x, y], label, label_align)
    return ax


def fixed_world(ax, pos, scale=1):
    p2 = mpatches.Rectangle((pos[0]-1*scale,pos[1]-1.5*scale), 1.0*scale, 3*scale, color='white', ec='none', zorder=2)
    ax.add_patch(p2)
    x,y = np.array([[
        pos[0]-1.*scale, 
        pos[0]-0.5*scale,
        ], [
        pos[1]-1*scale, 
        pos[1]-1.5*scale, 
        ]])
    line = mlines.Line2D(x, y, lw=1.75*scale, color='k', solid_capstyle='round', zorder=2)
    ax.add_line(line)
    x,y = np.array([[
        pos[0]-1*scale, 
        pos[0],
        ], [
        pos[1]+1*scale, 
        pos[1], 
        ]])
    line = mlines.Line2D(x, y, lw=1.75*scale, color='k', solid_capstyle='round', zorder=2)
    ax.add_line(line)
    x,y = np.array([[
        pos[0]-1*scale, 
        pos[0],
        ], [
        pos[1], 
        pos[1]-1*scale, 
        ]])
    line = mlines.Line2D(x, y, lw=1.75*scale, color='k', solid_capstyle='round', zorder=2)
    ax.add_line(line)
    x,y = np.array([[
        pos[0]-0.5*scale, 
        pos[0],
        ], [
        pos[1]+1.5*scale, 
        pos[1]+1.*scale, 
        ]])
    line = mlines.Line2D(x, y, lw=1.75*scale, color='k', solid_capstyle='round', zorder=2)
    ax.add_line(line)
    x,y = np.array([[
        pos[0], 
        pos[0],
        ], [
        pos[1]-1.5*scale, 
        pos[1]+1.5*scale, 
        ]])
    line = mlines.Line2D(x, y, lw=2.5*scale, color='k', solid_capstyle='round', zorder=2)
    ax.add_line(line)
    return ax


def fixed_world_r(ax, pos, scale=1):
    p2 = mpatches.Rectangle((pos[0]*scale,pos[1]-1.5*scale), 1.0*scale, 3*scale, color='white', ec='none', zorder=2)
    ax.add_patch(p2)
    x,y = np.array([[
        pos[0]+1.*scale, 
        pos[0]+0.5*scale,
        ], [
        pos[1]-1*scale, 
        pos[1]-1.5*scale, 
        ]])
    line = mlines.Line2D(x, y, lw=1.75*scale, color='k', solid_capstyle='round', zorder=2)
    ax.add_line(line)
    x,y = np.array([[
        pos[0]+1*scale, 
        pos[0],
        ], [
        pos[1]+1*scale, 
        pos[1], 
        ]])
    line = mlines.Line2D(x, y, lw=1.75*scale, color='k', solid_capstyle='round', zorder=2)
    ax.add_line(line)
    x,y = np.array([[
        pos[0]+1*scale, 
        pos[0],
        ], [
        pos[1], 
        pos[1]-1*scale, 
        ]])
    line = mlines.Line2D(x, y, lw=1.75*scale, color='k', solid_capstyle='round', zorder=2)
    ax.add_line(line)
    x,y = np.array([[
        pos[0]+0.5*scale, 
        pos[0],
        ], [
        pos[1]+1.5*scale, 
        pos[1]+1.*scale, 
        ]])
    line = mlines.Line2D(x, y, lw=1.75*scale, color='k', solid_capstyle='round', zorder=2)
    ax.add_line(line)
    x,y = np.array([[
        pos[0], 
        pos[0],
        ], [
        pos[1]-1.5*scale, 
        pos[1]+1.5*scale, 
        ]])
    line = mlines.Line2D(x, y, lw=2.5*scale, color='k', solid_capstyle='round', zorder=2)
    ax.add_line(line)
    return ax


def source_of_displacement(ax, pos, label='', placement='top'):
    verts1 = [
        (pos[0]-1.5, pos[1]+3),
        (pos[0]-1.5, pos[1]-3), #P0
        (pos[0]-1.5, pos[1]), #P1
        (pos[0]+1.5, pos[1]), #P2
        (pos[0]+1.5, pos[1]+3), #P3
        (pos[0]-1.5, pos[1]+3),
        ]
    
    codes1 = [mpath.Path.MOVETO,
             mpath.Path.LINETO,
             mpath.Path.CURVE4,
             mpath.Path.CURVE4,
             mpath.Path.CURVE4,
             mpath.Path.CLOSEPOLY,
             ]
    
    path1 = mpath.Path(verts1, codes1)
    patch1 = mpatches.PathPatch(path1, edgecolor='w', facecolor='w', lw=0.5, zorder=2)
    ax.add_patch(patch1)
    verts1 = [
        (pos[0]-1.5, pos[1]+3),
        (pos[0]-1.5, pos[1]-3), #P0
        (pos[0]-1.5, pos[1]), #P1
        (pos[0]+1.5, pos[1]), #P2
        (pos[0]+1.5, pos[1]+3), #P3
        ]
    
    codes1 = [mpath.Path.MOVETO,
             mpath.Path.LINETO,
             mpath.Path.CURVE4,
             mpath.Path.CURVE4,
             mpath.Path.CURVE4,
             ]
    
    path1 = mpath.Path(verts1, codes1)
    patch1 = mpatches.PathPatch(path1, edgecolor='k', facecolor='none', lw=2.5, zorder=2)
    ax.add_patch(patch1)
    if label !='':
        if placement == 'center':
            x,y = pos[0], pos[1]
            label_align = 'center'
        elif placement == 'top':
            x,y = pos[0], pos[1]+4.5
            label_align = 'bottom'
        elif placement == 'bottom':
            x,y = pos[0], pos[1]-4
            label_align = 'top'
        else:
            return ax
        add_label([x, y], label, label_align)
    return ax

def source_of_force(ax, pos, forcelength=12, label='', placement='top'):
    x,y = np.array([[pos[0]+0.5*forcelength-1.5, pos[0]+0.5*forcelength-0.1, pos[0]+0.5*forcelength-1.5], [pos[1]-0.75, pos[1], pos[1]+0.75]])
    line = mlines.Line2D(x, y, lw=2.5, color='k', zorder=2, solid_capstyle='round')
    ax.add_line(line)
    x,y = np.array([[pos[0]-0.5*forcelength+1.5, pos[0]-0.5*forcelength+0.1, pos[0]-0.5*forcelength+1.5], [pos[1]-0.75, pos[1], pos[1]+0.75]])
    line = mlines.Line2D(x, y, lw=2.5, color='k', zorder=2, solid_capstyle='round')
    ax.add_line(line)
    x,y = np.array([[pos[0]-0.5*forcelength, pos[0]+0.5*forcelength], [pos[1], pos[1]]])
    line = mlines.Line2D(x, y, lw=1.75, color='k', zorder=2)
    ax.add_line(line)
    plt.text(pos[0], pos[1],r'$F$', horizontalalignment='center', verticalalignment='center',backgroundcolor='white',
        fontsize=20, family='serif')
    if label !='':
        if placement == 'center':
            x,y = pos[0], pos[1]
            label_align = 'center'
        elif placement == 'top':
            x,y = pos[0], pos[1]+2.5
            label_align = 'bottom'
        elif placement == 'bottom':
            x,y = pos[0], pos[1]-2
            label_align = 'top'
        else:
            return ax
        add_label([x, y], label, label_align)
    return ax


def displacement(ax, pos, label='', placement='bottom'):
    if placement == 'bottom':
        x1,y1 = np.array([[pos[0], pos[0], pos[0]+3], [pos[1]-3.5, pos[1]-5, pos[1]-5]])
        x2,y2 = np.array([[pos[0]+2, pos[0]+3.1, pos[0]+2], [pos[1]-4.5, pos[1]-5, pos[1]-5.5]])
        x3,y3 = pos[0]+2, pos[1]-6
        label_align = 'top'
    elif placement == 'top':
        x1,y1 = np.array([[pos[0], pos[0], pos[0]+3], [pos[1]+3.5, pos[1]+5, pos[1]+5]])
        x2,y2 = np.array([[pos[0]+2, pos[0]+3.1, pos[0]+2], [pos[1]+4.5, pos[1]+5, pos[1]+5.5]])
        x3,y3 = pos[0]+2, pos[1]+6
        label_align = 'bottom'
    else:
        return ax

    line = mlines.Line2D(x1, y1, lw=1.5, color='k', zorder=2, solid_capstyle='round')
    ax.add_line(line)
    line = mlines.Line2D(x2, y2, lw=1.5, color='k', zorder=2, solid_capstyle='round')
    ax.add_line(line)
    plt.text(x3, y3,label, horizontalalignment='center', verticalalignment=label_align,
        fontsize=20, family='serif')
    return ax


def source_of_torque(ax, pos, torquelength=12, label='', placement='top'):
    x,y = np.array([[pos[0]+0.5*torquelength-1.5, pos[0]+0.5*torquelength-0.1, pos[0]+0.5*torquelength-1.5], [pos[1]-0.75, pos[1], pos[1]+0.75]])
    line = mlines.Line2D(x, y, lw=2.5, color='k', zorder=2, solid_capstyle='round')
    ax.add_line(line)
    x,y = np.array([[pos[0]+0.5*torquelength-2.5, pos[0]+0.5*torquelength-1.1, pos[0]+0.5*torquelength-2.5], [pos[1]-0.75, pos[1], pos[1]+0.75]])
    line = mlines.Line2D(x, y, lw=2.5, color='k', zorder=2, solid_capstyle='round')
    ax.add_line(line)
    x,y = np.array([[pos[0]-0.5*torquelength+1.5, pos[0]-0.5*torquelength+0.1, pos[0]-0.5*torquelength+1.5], [pos[1]-0.75, pos[1], pos[1]+0.75]])
    line = mlines.Line2D(x, y, lw=2.5, color='k', zorder=2, solid_capstyle='round')
    ax.add_line(line)
    x,y = np.array([[pos[0]-0.5*torquelength+2.5, pos[0]-0.5*torquelength+1.1, pos[0]-0.5*torquelength+2.5], [pos[1]-0.75, pos[1], pos[1]+0.75]])
    line = mlines.Line2D(x, y, lw=2.5, color='k', zorder=2, solid_capstyle='round')
    ax.add_line(line)
    x,y = np.array([[pos[0]-0.5*torquelength, pos[0]+0.5*torquelength], [pos[1], pos[1]]])
    line = mlines.Line2D(x, y, lw=1.75, color='k', zorder=2)
    ax.add_line(line)
    plt.text(pos[0], pos[1],r'$T$', horizontalalignment='center', verticalalignment='center',backgroundcolor='white',
        fontsize=20, family='serif')
    if label !='':
        if placement == 'center':
            x,y = pos[0], pos[1]
            label_align = 'center'
        elif placement == 'top':
            x,y = pos[0], pos[1]+2.5
            label_align = 'bottom'
        elif placement == 'bottom':
            x,y = pos[0], pos[1]-2
            label_align = 'top'
        else:
            return ax
        add_label([x, y], label, label_align)
    return ax


def lever_less(ax, pos, leverlength=8, label='', placement='top'):
    circle = mpatches.Circle((pos[0], pos[1]), 0.5, facecolor='w',
                edgecolor='k', linewidth=1.75, zorder=3)
    ax.add_patch(circle)
    circle = mpatches.Circle((pos[0], pos[1]-leverlength), 0.5, facecolor='w',
                edgecolor='k', linewidth=1.75, zorder=3)
    ax.add_patch(circle)
    halfcircle = mpatches.Wedge((pos[0], pos[1]-0.5*leverlength), 0.5, -90, 90, facecolor='w',
                edgecolor='k', linewidth=1.75, zorder=3)
    ax.add_patch(halfcircle)
    x,y = np.array([[
        pos[0], 
        pos[0],
        ], [
        pos[1], 
        pos[1]-leverlength, 
        ]])
    line = mlines.Line2D(x, y, lw=2.5, color='k', solid_capstyle='round', zorder=2)
    ax.add_line(line)
    x,y = np.array([[
        pos[0]-1, 
        pos[0],
        pos[0]-1, 
        ], [
        pos[1]-leverlength-0.75, 
        pos[1]-leverlength, 
        pos[1]-leverlength+0.75, 
        ]])
    line = mlines.Line2D(x, y, lw=1.75, color='k', solid_capstyle='round', zorder=2)
    ax.add_line(line)
    fixed_world(ax, [pos[0]-1, pos[1]-leverlength], 1.75/2.5)
    if label !='':
        if placement == 'center':
            x,y = pos[0], pos[1]-0.5*leverlength
            label_align = 'center'
        elif placement == 'top':
            x,y = pos[0], pos[1]+1.5
            label_align = 'bottom'
        elif placement == 'bottom':
            x,y = pos[0], pos[1]-leverlength-2
            label_align = 'top'
        else:
            return ax
        add_label([x, y], label, label_align)
    return ax


def lever_more(ax, pos, leverlength=8, label='', placement='top'):
    circle = mpatches.Circle((pos[0], pos[1]+0.5*leverlength), 0.5, facecolor='w',
                edgecolor='k', linewidth=1.75, zorder=3)
    ax.add_patch(circle)
    circle = mpatches.Circle((pos[0], pos[1]-0.5*leverlength), 0.5, facecolor='w',
                edgecolor='k', linewidth=1.75, zorder=3)
    ax.add_patch(circle)
    halfcircle = mpatches.Wedge((pos[0], pos[1]), 0.5, 90, 270, facecolor='w',
                edgecolor='k', linewidth=1.75, zorder=3)
    ax.add_patch(halfcircle)
    x,y = np.array([[
        pos[0], 
        pos[0],
        ], [
        pos[1]+0.5*leverlength, 
        pos[1]-0.5*leverlength, 
        ]])
    line = mlines.Line2D(x, y, lw=2.5, color='k', solid_capstyle='round', zorder=2)
    ax.add_line(line)
    x,y = np.array([[
        pos[0]-1, 
        pos[0],
        pos[0]-1, 
        ], [
        pos[1]-0.5*leverlength-0.75, 
        pos[1]-0.5*leverlength, 
        pos[1]-0.5*leverlength+0.75, 
        ]])
    line = mlines.Line2D(x, y, lw=1.75, color='k', solid_capstyle='round', zorder=2)
    ax.add_line(line)
    fixed_world(ax, [pos[0]-1, pos[1]-0.5*leverlength], 1.75/2.5)
    if label !='':
        if placement == 'center':
            x,y = pos[0], pos[1]
            label_align = 'center'
        elif placement == 'top':
            x,y = pos[0], pos[1]+0.5*leverlength+1.5
            label_align = 'bottom'
        elif placement == 'bottom':
            x,y = pos[0], pos[1]-0.5*leverlength-2
            label_align = 'top'
        else:
            return ax
        add_label([x, y], label, label_align)
    return ax


def motion_summer(ax, pos, leverlength=8):
    circle = mpatches.Circle((pos[0]-2, pos[1]+0.5*leverlength), 0.5, facecolor='w',
                edgecolor='k', linewidth=1.75, zorder=3)
    ax.add_patch(circle)
    circle = mpatches.Circle((pos[0]-2, pos[1]-0.5*leverlength), 0.5, facecolor='w',
                edgecolor='k', linewidth=1.75, zorder=3)
    ax.add_patch(circle)
    x,y = np.array([[
        pos[0]-2, 
        pos[0]+2,
        ], [
        pos[1], 
        pos[1], 
        ]])
    line = mlines.Line2D(x, y, lw=1.75, color='k', solid_capstyle='round', zorder=2)
    ax.add_line(line)
    halfcircle = mpatches.Wedge((pos[0]-2, pos[1]), 0.5, -90, 90, facecolor='w',
                edgecolor='k', linewidth=1.75, zorder=3)
    ax.add_patch(halfcircle)
    x,y = np.array([[
        pos[0]-2, 
        pos[0]-2,
        ], [
        pos[1]+0.5*leverlength, 
        pos[1]-0.5*leverlength, 
        ]])
    line = mlines.Line2D(x, y, lw=2.5, color='k', solid_capstyle='round', zorder=2)
    ax.add_line(line)
    lever_more(ax, [pos[0]+2, pos[1]], leverlength)
    return ax


def connector(ax, datapoints):
    xdata = []
    ydata = []
    for v in datapoints:
        xdata.append(v[0])
        ydata.append(v[1])
    line = mlines.Line2D(xdata, ydata, lw=1.5, color='k', zorder=1)
    ax.add_line(line)
    return ax


def iconic_diagram_figure(num=None, figsize=(15, 7.5), dpi=150, facecolor='w', edgecolor='k'):
    fig = plt.figure(num,figsize,dpi,facecolor,edgecolor)
    ax = get_ic_axes(fig)
    ax.axes.get_xaxis().set_visible(False)     # Hide x axis
    ax.axes.get_yaxis().set_visible(False)     # Hide y axis

    ax.axis([0, 100, 0, 50])

    return fig